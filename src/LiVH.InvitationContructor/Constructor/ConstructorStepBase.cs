using System.Collections.Generic;
using CSharpFunctionalExtensions;

namespace LiVH.InvitationConstructor.Constructor
{
    public abstract class ConstructorStepBase : ValueObject
    {
        public abstract string Name { get; }
        protected override IEnumerable<object> GetEqualityComponents()
        {
            throw new System.NotImplementedException();
        }
    }
}