﻿using LiVH.Core.DataContract.Calendar.Reservation;
using LiVH.Core.DataContract.Product;
using LiVH.DAL.Contracts;
using LiVH.DAL.Contracts.Interfaces;
using Microsoft.Extensions.Logging;

namespace LiVH.Services.Product
{
    public class ProtographyProductService: ProductServiceBase<ProductPhotography>
    {
        public ProtographyProductService(IProductRepository<ProductPhotography> productRepository, ICalendarRepository<ReservationCalendarInfo> reservationСalendarRepository, IPortfolioRepository portfolioRepository, ILogger logger) : base(productRepository, reservationСalendarRepository, portfolioRepository, logger)
        {
        }
    }
}