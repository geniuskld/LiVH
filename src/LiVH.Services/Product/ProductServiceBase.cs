﻿using System;
using System.Linq;
using System.Transactions;
using LiVH.Core.Common;
using LiVH.Core.DataContract.Calendar;
using LiVH.Core.DataContract.Calendar.Reservation;
using LiVH.Core.DataContract.Product;
using LiVH.Core.DataContract.Product.Portfolio;
using LiVH.DAL.Contracts;
using LiVH.DAL.Contracts.Interfaces;
using Microsoft.Extensions.Logging;

namespace LiVH.Services.Product
{
    public abstract class ProductServiceBase<T> : EntityOrientedServiceBase<T> where T : ProductBase
    {
        private readonly ICalendarRepository<ReservationCalendarInfo> _reservationCalendarRepository;
        private readonly IPortfolioRepository _portfolioRepository;

        protected ProductServiceBase(IProductRepository<T> productRepository,
            ICalendarRepository<ReservationCalendarInfo> reservationСalendarRepository,
            IPortfolioRepository portfolioRepository,
            ILogger logger) : base(productRepository, logger)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            _reservationCalendarRepository = reservationСalendarRepository ?? throw new ArgumentNullException(nameof(reservationСalendarRepository));
            _portfolioRepository = portfolioRepository ?? throw new ArgumentNullException(nameof(portfolioRepository));
        }

        public override OperationResult<T> Create(T entry)
        {
            if (entry.ClientId <= 0 && entry.Client == null)
                throw new ArgumentOutOfRangeException("Client id is required");

            return base.Create(entry);
        }


        #region  Album and portfolio

        public virtual Portfolio CreatePortfolio(long productId)
        {
            if (productId <= 0) throw new ArgumentOutOfRangeException(nameof(productId));

            try
            {
                var portfolio = _portfolioRepository.CreatePortfolio(productId);
                return portfolio;
            }
            catch (Exception e)
            {
                Logger.LogError("Exception during saving portfolio", e);
                throw;
            }
        }

        public virtual Album CreateAlbum(long portfolioId)
        {
            if (portfolioId <= 0) throw new ArgumentOutOfRangeException(nameof(portfolioId));

            var album = new Album {PortfolioId = portfolioId};

            try
            {
                _portfolioRepository.CreateAlbum(album, portfolioId);
            }
            catch (Exception e)
            {
                Logger.LogError("Exception during creating album", e);
                throw;
            }

            return album;
        }

        public virtual void RemoveAlbum(long albumId)
        {
            if (albumId <= 0) throw new ArgumentOutOfRangeException(nameof(albumId));

            try
            {
                _portfolioRepository.RemoveAlbum(albumId);
            }
            catch (Exception e)
            {
                Logger.LogError($"Exception while removing album {albumId}", e);
                throw;
            }
        }

        public virtual AlbumItem AddToAlbum(long albumId, AlbumItem item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            if (item.Url == null) throw new ArgumentNullException(nameof(item.Url));
            if (item.Url.Urls == null) throw new ArgumentNullException(nameof(item.Url.Urls));
            if (!item.Url.Urls.Any())
                throw new ArgumentOutOfRangeException("Album Urls have to have atleast one element");

            try
            {
                return _portfolioRepository.AddToAlbum(item, albumId);
            }
            catch (Exception e)
            {
                Logger.LogError("Exception while adding item to album");
                throw;
            }
        }

        #endregion

        #region Calendar

        public OperationResult<CalendarEntry<ReservationCalendarInfo>> MakeReservation(long productId,
            ReservationCalendarEntry entry,
            DateTime reservationExpires)
        {
            var calendar = _reservationCalendarRepository.GetByProductId(productId);
            entry.CalendarId = calendar.Id;
            entry.EntryExpiresOn = reservationExpires;

            OperationResult<CalendarEntry<ReservationCalendarInfo>> entryResult;
            
            using (var t = new TransactionScope())
            {
                entryResult = _reservationCalendarRepository.AddEntry(entry);
                t.Complete();
            }

            return entryResult;
        }

        public IQueryable<T> GetAviableActualProductsByDateRange(DateTime startDate, DateTime endDate)
        {
            if (endDate <= startDate) throw new ArgumentOutOfRangeException("startDate <= endDate");

            var notOverlappedDates = Repository.GetAll().Where(x => x.ReservationCalendar != null)
                .SelectMany(x => x.ReservationCalendar.Entries).Where(x =>
                    x.Ending > DateTime.UtcNow && x.EntryExpiresOn <= DateTime.UtcNow)
                .Where(x => x.Beginning <= endDate && x.Beginning >= startDate)
                .Select(x => x.CalendarBase.Product as T);

            return notOverlappedDates;
        }

        #endregion
    }
}