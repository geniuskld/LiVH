﻿using System;
using System.Linq;
using LiVH.Core.DataContract.Product;
using LiVH.DAL.Contracts.Interfaces;
using LiVH.Services.DTO;

namespace LiVH.Services.Product
{
    public class ProductSearchService
    {
        private readonly IProductRepository<ProductBase> _productsRepository;

        public ProductSearchService(IProductRepository<ProductBase> productsRepository)
        {
            _productsRepository = productsRepository ?? throw new ArgumentNullException(nameof(productsRepository));
        }

        public IQueryable<ProductBase> Search(ProductSearchQuery searchQuery)
        {
            var queryResult = _productsRepository.GetAll();

            //Filter by non overlated dates
            if (queryResult.Any(x => x.AviabilityCalendarBase != null) &&
                searchQuery.AviableDateStart < searchQuery.AviableDateEnd)
            {
                queryResult = _productsRepository.GetAll().Where(x => x.ReservationCalendar != null)
                    .SelectMany(x => x.ReservationCalendar.Entries).Where(x =>
                        x.Ending > DateTime.UtcNow && x.EntryExpiresOn <= DateTime.UtcNow)
                    .Where(x => x.Beginning <= searchQuery.AviableDateEnd && x.Beginning >= searchQuery.AviableDateStart )
                    .Select(x => x.CalendarBase.Product);
            }

            //If price calendar exists search prices for our dates
            if (queryResult.Any(x => x.PriceCalendarBase != null))
            {
                
            }

            return queryResult;
        }
    }
}