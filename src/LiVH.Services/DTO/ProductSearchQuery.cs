﻿using System;
using System.Collections.Generic;
using LiVH.Core.DataContract.Product.Category;

namespace LiVH.Services.DTO
{
    public class ProductSearchQuery
    {
        public DateTime AviableDateStart { get; set; }
        public DateTime AviableDateEnd { get; set; }
        public decimal PriceMin { get; set; }
        public decimal PriceMax { get; set; }
        public IEnumerable<ProductCategoryType> ProductCategories { get; set; }
    }
}