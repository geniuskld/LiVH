﻿using System;
using LiVH.Core.Common;
using LiVH.Core.DataContract.Calendar;
using LiVH.DAL.Contracts.Interfaces;
using Microsoft.Extensions.Logging;

namespace LiVH.Services.Calendar
{
    public class ProductCalendar : EntityOrientedServiceBase<CalendarBase>
    {
        private readonly IEntityOrientedRepository<CalendarEntryBase> _calendarEntriesRepository;

        public ProductCalendar(IEntityOrientedRepository<CalendarBase> calendarRepository,
            IEntityOrientedRepository<CalendarEntryBase> calendarEntriesRepository,
            ILogger logger) : base(calendarRepository, logger)
        {
            _calendarEntriesRepository = calendarEntriesRepository ??
                                         throw new ArgumentNullException(nameof(calendarEntriesRepository));
        }

        public OperationResult<CalendarEntryBase> AddEntry(CalendarEntryBase entry, long calendarId)
        {
            if (entry == null) throw new ArgumentNullException(nameof(entry));
            if (calendarId <= 0) throw new ArgumentOutOfRangeException(nameof(calendarId));
            if (entry.CalendarId <= 0) throw new ArgumentOutOfRangeException(nameof(calendarId));

            return _calendarEntriesRepository.Create(entry);
        }

        public OperationResult<CalendarEntryBase> UpdateEntry(CalendarEntryBase entry)
        {
            if (entry == null) throw new ArgumentNullException(nameof(entry));
            if (entry.CalendarId <= 0) throw new ArgumentOutOfRangeException(nameof(entry));

            return _calendarEntriesRepository.Update(entry);
        }

        public OperationResult<CalendarEntryBase> RemoveEntry(long id)
        {
            return _calendarEntriesRepository.Remove(id);
        }

        public override OperationResult<CalendarBase> Remove(
            CalendarBase item)
        {
            return new OperationResult<CalendarBase> {State = OperationResultState.Error};
        }

        public override OperationResult<CalendarBase> Remove(long id)
        {
            return new OperationResult<CalendarBase> {State = OperationResultState.Error};
        }
    }
}