﻿using System;
using System.Linq;
using LiVH.Core.DataContract.Client;
using LiVH.DAL.Contracts.Interfaces;
using Microsoft.Extensions.Logging;

namespace LiVH.Services.Client
{
    public class ClientsService : EntityOrientedServiceBase<Core.DataContract.Client.Client>
    {
        private readonly IEntityOrientedRepository<Core.DataContract.Client.Client> _clientRepository;
        private readonly IEntityOrientedRepository<User> _userRepository;
        private readonly IEntityOrientedRepository<UserToClient> _userToClientAssociatedRepository;
        private readonly ILogger _logger;

        public ClientsService(IEntityOrientedRepository<Core.DataContract.Client.Client> calendarRepository,
            IEntityOrientedRepository<User> userRepository,
            IEntityOrientedRepository<UserToClient> userToClientAssociatedRepository, ILogger logger) :
            base(calendarRepository, logger)
        {
            _clientRepository = calendarRepository ?? throw new ArgumentNullException(nameof(calendarRepository));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userToClientAssociatedRepository = userToClientAssociatedRepository ??
                                                throw new ArgumentNullException(
                                                    nameof(userToClientAssociatedRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void AssociateClientToUser(Core.DataContract.Client.Client client, User user, OwnType ownership)
        {
            if (client == null) throw new ArgumentNullException(nameof(client));
            if (user == null) throw new ArgumentNullException(nameof(user));
            if (ownership == OwnType.Unset) throw new ArgumentOutOfRangeException(nameof(ownership));

            //Find same asscociation
            var association = _userToClientAssociatedRepository.GetAll()
                .FirstOrDefault(x => x.UserId == user.Id && x.ClientId == client.Id);

            if (association != null)
            {
                association.OwnRight = ownership;
            }

            _userToClientAssociatedRepository.Update(association);
        }

        public void DissociateClientAndUser(Core.DataContract.Client.Client client, User user)
        {
            if (client == null) throw new ArgumentNullException(nameof(client));
            if (user == null) throw new ArgumentNullException(nameof(user));
            
            var association = _userToClientAssociatedRepository.GetAll()
                .FirstOrDefault(x => x.UserId == user.Id && x.ClientId == client.Id);
            
            if (association != null)
            {
                _userToClientAssociatedRepository.Remove(association);
            }
        }
    }
}