﻿using System;
using System.Linq;
using LiVH.Core.Common;
using LiVH.Core.DataContract;
using LiVH.DAL.Contracts.Interfaces;
using Microsoft.Extensions.Logging;

namespace LiVH.Services
{
    public abstract class EntityOrientedServiceBase<T> : ServiceBase where T : IEntity
    {
        protected readonly IEntityOrientedRepository<T> Repository;

        protected EntityOrientedServiceBase(IEntityOrientedRepository<T> calendarRepository, ILogger logger) : base(logger)
        {
            Repository = calendarRepository ?? throw new ArgumentNullException(nameof(calendarRepository));
        }

        public virtual IQueryable<T> GetAll()
        {
            return Repository.GetAll();
        }

        public virtual T GetById(long id)
        {
            return Repository.GetById(id);
        }
        
        public virtual OperationResult<T> Create(T entry)
        {
            return Repository.Create(entry);
        }

        public virtual OperationResult<T> Update(T item)
        {
            return Repository.Update(item);
        }

        public virtual OperationResult<T> Remove(T item)
        {
            return Repository.Remove(item);
        }

        public virtual OperationResult<T> Remove(long id)
        {
            return Repository.Remove(id);
        }
    }
}