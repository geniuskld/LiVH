﻿using System;

namespace LiVH.Core.Common
{
    public class OperationResult<T>
    {
        public OperationResultState State { get; set; }
        public T Result { get; set; }
        public Exception Exception { get; set; }
        public string Description { get; set; }
    }
}