﻿namespace LiVH.Core.Common
{
    public enum OperationResultState
    {
        Unset=0,
        OK,
        Error
    }
}