﻿namespace LiVH.Core.DataContract
{
    public interface IMutableEntity : IEntity

    {
        bool CanBeModified();
    }
}