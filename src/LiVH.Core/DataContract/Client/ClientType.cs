﻿namespace LiVH.Core.DataContract.Client
{
    public enum ClientType : long
    {
        Unset = 0,
        Couple = 1,
        Photographer = 10,
        Videographer = 11,
        Store = 12,
        Organizer = 13,
        Agency = 14,
        Stylist = 15,
        CarRental = 16,
        Restaurant = 20,
        
    }
}