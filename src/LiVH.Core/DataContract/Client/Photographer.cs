﻿using LiVH.Core.DataContract.Product;

namespace LiVH.Core.DataContract.Client
{
    public class Photographer : Client
    {
        public Photographer()
        {
            ClientType = ClientType.Photographer;
        }
    }
}