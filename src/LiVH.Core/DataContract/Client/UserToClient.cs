﻿namespace LiVH.Core.DataContract.Client
{
    public class UserToClient : IEntity
    {
        public long ClientId { get; set; }
        public long UserId { get; set; }
        public OwnType OwnRight { get; set; }
        
        public virtual Client Client { get; set; }
        public virtual User User { get; set; }
        public long Id { get; set; }
    }
}