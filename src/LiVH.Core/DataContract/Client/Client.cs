﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using LiVH.Core.DataContract.Common;

namespace LiVH.Core.DataContract.Client
{
    /// <summary>
    /// Base client entity
    /// </summary>
    public abstract class Client : TimeBoundEntity
    {
        [DataType(DataType.Text)] public TranslateableString Name { get; set; }
        [DataType(DataType.Text)] public TranslateableString Description { get; set; }
        public ClientType ClientType { get; set; }
        
        /// <summary>
        /// Many users can own client
        /// </summary>
        public virtual ICollection<User> Representatives { get; set; }
    }
}