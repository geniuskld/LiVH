﻿namespace LiVH.Core.DataContract.Client
{
    public enum OwnType
    {
        Unset = 0,
        Owner = 1,
        Reader = 2
    }
}