﻿namespace LiVH.Core.DataContract.Client
{
    public enum SocialNetworkType
    {
        Unset = 0,
        Facebook = 1,
        Vk = 2
    }
}