﻿using System;

namespace LiVH.Core.DataContract.Client
{
    public class SocialNetworkAccount : TimeBoundEntity
    {
        public SocialNetworkType Network { get; set; }
        public string Login { get; set; }
        public string Token { get; set; }
        public DateTime TokenValidTill { get; set; }
        public string SerializedInfo { get; set; }
    }
}