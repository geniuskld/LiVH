﻿using System.Collections.Generic;

namespace LiVH.Core.DataContract.Client
{
    /// <summary>
    /// Authorization profile of client
    /// </summary>
    public class User : TimeBoundEntity
    {
        public string Password { get; set; }
        public virtual ICollection<Client> ClientsOwnedByUser { get; set; }
        public ICollection<SocialNetworkAccount> SocialNetworkAccounts { get; set; }
    }
}