﻿using System;

namespace LiVH.Core.DataContract.Calendar.Reservation
{
    public class ReservationCalendarInfo
    {
        public bool IsAviable { get; set; }
        public long ReservationId { get; set; }
        public DateTime ReservationExpires { get; set; }
    }
}