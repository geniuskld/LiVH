﻿using System.Collections.Generic;
using LiVH.Core.DataContract.Product;

namespace LiVH.Core.DataContract.Calendar
{
    public abstract class CalendarBase : IEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        
        public long ProductId { get; set; }
        public virtual ProductBase Product { get; set; }
    }
}