﻿using System;
using LiVH.Core.DataContract.Product;

namespace LiVH.Core.DataContract.Calendar
{
    public class CalendarEntry<T> : CalendarEntryBase
    {
        public T Info { get; set; }
    }
}