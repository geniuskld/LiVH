﻿using System;

namespace LiVH.Core.DataContract.Calendar
{
    public abstract class CalendarEntryBase : TimeBoundEntity, IMutableEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Beginning { get; set; }
        public DateTime Ending { get; set; }
        public DateTime EntryExpiresOn { get; set; }
        public long CalendarId { get; set; }
        
        public virtual CalendarBase CalendarBase { get; set; }
        
        public virtual bool CanBeModified()
        {
            throw new NotImplementedException();
        }
    }
}