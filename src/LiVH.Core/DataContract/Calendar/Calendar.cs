﻿using System.Collections.Generic;

namespace LiVH.Core.DataContract.Calendar
{
    public class Calendar<T> : CalendarBase
    { 
        public virtual ICollection<CalendarEntry<T>> Entries { get; set; }
    }
}