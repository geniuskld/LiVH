﻿using LiVH.Core.DataContract.Common;

namespace LiVH.Core.DataContract.Product.Portfolio
{
    public class AlbumItem : TimeBoundEntity
    {
        public RessourceUrl Url { get; set; }
    }
}