﻿using System.Collections.Generic;
using LiVH.Core.DataContract.Common;

namespace LiVH.Core.DataContract.Product.Portfolio
{
    public class Album: TimeBoundEntity
    {
        public long PortfolioId { get; set; }
        public TranslateableString Name { get; set; }
        public TranslateableString Description { get; set; }
        private ICollection<AlbumItem> Items { get; set; }
    }
}