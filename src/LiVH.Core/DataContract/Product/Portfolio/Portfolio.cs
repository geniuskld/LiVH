﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace LiVH.Core.DataContract.Product.Portfolio
{
    public class Portfolio : IEntity
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public ICollection<Album> Albums { get; set; }
        
        [ForeignKey("ProdictId")]
        public virtual ProductBase Product { get; set; }
    }
}