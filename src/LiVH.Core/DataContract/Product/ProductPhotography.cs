﻿using LiVH.Core.DataContract.Product.Category;

namespace LiVH.Core.DataContract.Product
{
    public class ProductPhotography : ProductBase
    {
        public ProductPhotography()
        {
            CategoryType = ProductCategoryType.Photography;
        }

        public Portfolio.Portfolio Portfolio { get; set; }
    }
}