﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;
using LiVH.Core.DataContract.Calendar;
using LiVH.Core.DataContract.Calendar.PriceCalendar;
using LiVH.Core.DataContract.Calendar.Reservation;
using LiVH.Core.DataContract.Common;
using LiVH.Core.DataContract.Product.Category;

namespace LiVH.Core.DataContract.Product
{
    public abstract class ProductBase : TimeBoundEntity
    {
        protected ProductBase()
        {
            ReservationCalendar = new ReservationCalendar();
            Contract.EndContractBlock();
        }
        
        public long CategoryId { get; set; }
        public ProductCategoryType CategoryType { get; set; }
        [DataType(DataType.Text)] public TranslateableString Title { get; set; }
        [DataType(DataType.Text)] public TranslateableString Description { get; set; }
        public bool IsAviable { get; set; }
        [DataType(DataType.Currency)] public decimal Price { get; set; }
        public ReservationCalendar ReservationCalendar { get; set; }
        public PriceCalendar PriceCalendarBase { get; set; }
        public CalendarBase AviabilityCalendarBase { get; set; }
        public long ClientId { get; set; }
        
        public virtual Client.Client Client { get; set; }
        public ProductCategory Category { get; set; }
    }
}