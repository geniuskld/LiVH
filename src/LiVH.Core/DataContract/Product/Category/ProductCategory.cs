﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using LiVH.Core.DataContract.Common;

namespace LiVH.Core.DataContract.Product.Category
{
    /// <summary>
    /// Product category
    /// </summary>
    public class ProductCategory : IEntity
    {
        public long Id { get; set; }
        public long ParentId { get; set; }
        public string UrlComponent { get; set; }
        public TranslateableString Name { get; set; }
        public TranslateableString Description { get; set; }
        public ProductCategoryType CategoryType { get; set; }
        
        [ForeignKey("ParentId")]
        public virtual ProductCategory Parent { get; set; }
        public virtual ICollection<ProductCategory> Children { get; set; }
        
    }
}