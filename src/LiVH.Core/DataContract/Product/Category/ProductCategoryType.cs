﻿namespace LiVH.Core.DataContract.Product.Category
{
    public enum ProductCategoryType : long
    {
        Unset = 0,
        Photography = 10,
        Videography = 11
    }
}