﻿using System;
using System.Diagnostics.Contracts;

namespace LiVH.Core.DataContract
{
    public abstract class TimeBoundEntity : IEntity
    {
        protected TimeBoundEntity()
        {
            Created = DateTime.UtcNow;
            Contract.EndContractBlock();
        }

        public long Id { get; set; }
        public DateTime Created { get; protected set; }
        public DateTime Updated { get; set; }
    }
}