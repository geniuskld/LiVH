﻿namespace LiVH.Core.DataContract.Common
{
    public enum RessourceType
    {
        Unset = 0,
        Thumb = 1,
        MediumImage = 2,
        LargeImage = 3,
    }
}