﻿using System.Collections.Generic;

namespace LiVH.Core.DataContract.Common
{
    public class RessourceUrl
    {
        public Dictionary<RessourceType, string> Urls { get; set; }
    }
}