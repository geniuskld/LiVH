﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace LiVH.Core.DataContract.Common
{
    [DataContract, ComplexType]
    public class TranslateableString
    {
        private readonly Dictionary<string, string> _values = new Dictionary<string, string>();
        private static readonly CultureInfo DefaultCulture = CultureInfo.CurrentCulture;

        public TranslateableString()
        {
        }

        /// <summary>
        /// Sets current culture value to translation dictionary
        /// </summary>
        /// <param name="value"></param>
        /// <exception cref="ArgumentException"></exception>
        public TranslateableString(string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new ArgumentException("Value cannot be null or empty.", nameof(value));
            _values.Add(DefaultCulture.ThreeLetterISOLanguageName, value);
        }

        public string GetTranslation(CultureInfo culture)
        {
            var key = culture.ThreeLetterISOLanguageName;
            if (_values.ContainsKey(key))
            {
                return _values[key];
            }

            return null;
        }

        public void AddTranslation(CultureInfo culture, string value)
        {
            if (culture == null) throw new ArgumentNullException(nameof(culture));
            if (string.IsNullOrEmpty(value))
                throw new ArgumentException("Value cannot be null or empty.", nameof(value));

            var key = culture.ThreeLetterISOLanguageName;
            _values.Add(key, value);
        }

        public static implicit operator string(TranslateableString entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            return JsonConvert.SerializeObject(entity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Empty string cannot be set</exception>
        /// <exception cref="InvalidCastException">Use AddTranslation to set value</exception>
        public static implicit operator TranslateableString(string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new ArgumentException("Value cannot be null or empty.", nameof(value));
            try
            {
                return JsonConvert.DeserializeObject<TranslateableString>(value);
            }
            catch (Exception e)
            {
                //it could be a setter
                if (!value.StartsWith("{"))
                {
                    return new TranslateableString(value);
                }
            }

            throw new InvalidCastException("it should be non empty string");
        }
    }
}