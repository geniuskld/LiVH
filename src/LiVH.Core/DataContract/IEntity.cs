﻿namespace LiVH.Core.DataContract
{
    public interface IEntity
    {
        long Id { get; set; }
    }
}