﻿using System;
using System.Linq;
using LiVH.Core.Common;
using LiVH.Core.DataContract;
using LiVH.DAL.Contracts.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace LiVH.DAL.EFRepository
{
    public class EfEntityOrientedRepository<T> : IEntityOrientedRepository<T> where T : class, IEntity
    {
        private readonly LiVHContext _dataContext;
        private readonly DbSet<T> _dataSource;


        public EfEntityOrientedRepository(LiVHContext dataContext)
        {
            _dataContext = dataContext ?? throw new ArgumentNullException(nameof(dataContext));
            _dataSource = dataContext.Set<T>();
        }

        public T GetById(long id)
        {
            return _dataSource.Find(id);
        }

        public IQueryable<T> GetAll()
        {
            return _dataSource;
        }

        public OperationResult<T> Create(T entry)
        {
            var result = new OperationResult<T>();
            try
            {
                var newentry = _dataSource.Add(entry);
                _dataContext.SaveChanges();
                
                result.Result = newentry.Entity;
                result.State = OperationResultState.OK;
            }
            catch (Exception e)
            {
                result.State = OperationResultState.Error;
                result.Exception = e;
            }

            return result;
        }

        public OperationResult<T> Update(T entry)
        {
            var result = new OperationResult<T>();
            try
            {
                var newentry = _dataSource.Update(entry);
                _dataContext.SaveChanges();

                result.Result = newentry.Entity;
                result.State = OperationResultState.OK;
            }
            catch (Exception e)
            {
                result.State = OperationResultState.Error;
                result.Exception = e;
            }

            return result;
        }

        public OperationResult<T> Remove(T entry)
        {
            var result = new OperationResult<T>();
            try
            {
                var newentry = _dataSource.Remove(entry);
                _dataContext.SaveChanges();

                result.Result = newentry.Entity;
                result.State = OperationResultState.OK;
            }
            catch (Exception e)
            {
                result.State = OperationResultState.Error;
                result.Exception = e;
            }

            return result;
        }

        public OperationResult<T> Remove(long id)
        {
            var result = new OperationResult<T>();
            try
            {
                var entry = _dataSource.Find(id);
                var newentry = _dataSource.Remove(entry);
                _dataContext.SaveChanges();

                result.Result = newentry.Entity;
                result.State = OperationResultState.OK;
            }
            catch (Exception e)
            {
                result.State = OperationResultState.Error;
                result.Exception = e;
            }

            return result;
        }
    }
}