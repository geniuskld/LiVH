﻿using LiVH.Core.DataContract.Client;
using LiVH.Core.DataContract.Product;
using Microsoft.EntityFrameworkCore;

namespace LiVH.DAL.EFRepository
{
    public class LiVHContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<UserToClient> UserToClient { get; set; } 
        public DbSet<User> Users { get; set; }
        public DbSet<ProductBase> Products { get; set; }
    }
}