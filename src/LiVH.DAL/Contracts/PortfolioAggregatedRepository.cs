﻿using LiVH.Core.DataContract.Product.Portfolio;
using LiVH.DAL.Contracts.Interfaces;

namespace LiVH.DAL.Contracts
{
    public interface IPortfolioRepository : IRepository
    {
        Portfolio CreatePortfolio(long productId);
        Album CreateAlbum(Album album, long portfolioId);
        AlbumItem AddToAlbum(AlbumItem item, long albumId);

        void RemoveFromAlbum(long itemId);
        void RemoveAlbum(long albumId);

        void UpdateAlbum(Album album);
    }
}