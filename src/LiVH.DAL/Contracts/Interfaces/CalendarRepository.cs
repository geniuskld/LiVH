﻿using LiVH.Core.Common;
using LiVH.Core.DataContract.Calendar;

namespace LiVH.DAL.Contracts.Interfaces
{
    public interface ICalendarRepository<T> : IRepository
    {
        OperationResult<CalendarEntry<T>> AddEntry(CalendarEntry<T> entry);

        Calendar<T> GetByProductId(long productId);
    }
}