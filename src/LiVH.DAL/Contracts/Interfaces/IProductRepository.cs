﻿using LiVH.Core.DataContract.Product;

namespace LiVH.DAL.Contracts.Interfaces
{
    public interface IProductRepository<T>: IEntityOrientedRepository<T> where T: ProductBase
    {
        
    }
}