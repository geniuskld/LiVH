﻿using System.Linq;
using LiVH.Core.Common;
using LiVH.Core.DataContract;

namespace LiVH.DAL.Contracts.Interfaces
{
    public interface IEntityOrientedRepository<T> : IRepository where T : IEntity
    {
        T GetById(long id);
        IQueryable<T> GetAll();
        OperationResult<T> Create(T entry);
        OperationResult<T> Update(T entry);
        OperationResult<T> Remove(T entry);
        OperationResult<T> Remove(long id);
    }
}