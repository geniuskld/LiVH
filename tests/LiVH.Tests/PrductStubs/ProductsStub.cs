﻿using System;
using System.Collections.Generic;
using System.Globalization;
using FizzWare.NBuilder;
using LiVH.Core.DataContract.Calendar;
using LiVH.Core.DataContract.Calendar.PriceCalendar;
using LiVH.Core.DataContract.Calendar.Reservation;
using LiVH.Core.DataContract.Product;

namespace LiVH.Tests.PrductStubs
{
    public class ProductsStub
    {
        public static ICollection<ProductPhotography> GetProducts()
        {
            var prods = Builder<ProductPhotography>.CreateListOfSize(10).Build();

            var i = 0;
            foreach (var prod in prods)
            {
                prod.ReservationCalendar = new ReservationCalendar
                {
                    Entries = new List<CalendarEntry<ReservationCalendarInfo>>
                    {
                        new ReservationCalendarEntry
                        {
                            Beginning = DateTime.UtcNow.AddHours(i),
                            Ending = DateTime.UtcNow.AddDays(i),
                            Info = new ReservationCalendarInfo {IsAviable = false}
                        }
                    }
                };
                prod.PriceCalendarBase = new PriceCalendar
                {
                    Entries = new List<CalendarEntry<decimal>>
                    {
                        new CalendarEntry<decimal>
                        {
                            Beginning = DateTime.UtcNow.AddHours(i),
                            Ending = DateTime.UtcNow.AddDays(i),
                            Info = i * 100
                        }
                    }
                };

                i += 2;
            }

            return prods;
        }
    }
}