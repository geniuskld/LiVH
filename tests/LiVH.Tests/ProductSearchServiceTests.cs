﻿using System;
using System.Linq;
using FizzWare.NBuilder;
using LiVH.Core.DataContract.Product;
using LiVH.DAL.Contracts.Interfaces;
using LiVH.Services.DTO;
using LiVH.Services.Product;
using LiVH.Tests.PrductStubs;
using NSubstitute;
using Xunit;

namespace LiVH.Tests
{
    public class ProductSearchServiceTests
    {
        [Fact]
        public void SearchTest()
        {
            var repo = Substitute.For<IProductRepository<ProductBase>>();
            ProductBase pb = new ProductPhotography();

            var productsList = Builder<ProductPhotography>.CreateListOfSize(1000).Build();
            repo.GetAll().ReturnsForAnyArgs(ProductsStub.GetProducts().AsQueryable());
            
            var sut = new ProductSearchService(repo);

            var products = sut.Search(new ProductSearchQuery
            {
                AviableDateStart = DateTime.UtcNow,
                AviableDateEnd = DateTime.UtcNow.AddDays(1),
                PriceMin = 10,
                PriceMax = 20
            }).ToList();
            
            
        }
    }
}